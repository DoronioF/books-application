from django.contrib import admin
from Books.models import (
    Book,
    Magazine,
    Author,
    BookReview,
    Genre,
    Issue,
)

class BookAdmin(admin.ModelAdmin):
    pass

admin.site.register(Book, BookAdmin)

admin.site.register(Magazine)

admin.site.register(Author)

admin.site.register(BookReview)

admin.site.register(Genre)

admin.site.register(Issue)