from django.urls import path

from Books.magazins_views import(
    delete_magazine,
    show_magazines,
    create_magazine,
    show_magazine,
    update_magazine,
    delete_magazine,
    view_genre,
)

urlpatterns = [
    path("", show_magazines, name="show_magazines"),
    path("create-mag/", create_magazine, name="create_magazine"),
    path("<int:pk>", show_magazine,  name="show_magazine"),
    path("<int:pk>/edit/", update_magazine, name="edit_magazine"),
    path("<int:pk>/delete/", delete_magazine, name="delete_magazine"),
    path("genre/<int:pk>", view_genre, name="view_genre")
]