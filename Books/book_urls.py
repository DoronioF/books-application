from django.urls import path

from Books.book_views import(
    delete_book,
    show_books,
    create_book,
    show_book,
    update_book,
    delete_book,
)

urlpatterns = [
    path("", show_books, name="show_books"),
    path("create-mag/", create_book, name="create_book"),
    path("<int:pk>", show_book,  name="show_book"),
    path("<int:pk>/edit/", update_book, name="edit_book"),
    path("<int:pk>/delete/", delete_book, name="delete_book"),
]