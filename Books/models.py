from django.db import models
from django.utils.translation import gettext_lazy

class Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=100, null=True, unique=True)
    authors = models.ManyToManyField(Author, related_name="books")
    pages = models.IntegerField(null=True)
    isbn = models.IntegerField(null=True)
    image = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True)
    year_published = models.SmallIntegerField(null=True)
    description = models.CharField(max_length=2000, null=True)

    def __str__(self):
        return self.title + " by " + str(self.authors.first())


class BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    review_title = models.CharField(max_length=100, null=True)
    text = models.TextField()
    reviewer = models.CharField(max_length=50, null=True)

    def __str__(self):
        return f"{self.review_title} by {self.reviewer} for {str(self.book)}"

class Magazine(models.Model):
    class ReleaseCycle(models.TextChoices):
        quarterly = "QR", ('Quarterly')
        monthly = "MO", ('Monthly')
        weekly = "WK", ('Weekly')
    title = models.CharField(max_length=100, null=True, unique=True)
    cover_page = models.URLField(null=True, blank=True)
    page = models.IntegerField(null=True)
    release_cycle = models.CharField(max_length=10, null=True)
    description = models.CharField(max_length=1000, null=True)
    releasecycle = models.CharField(max_length=2, choices=ReleaseCycle.choices, default=None)

    def __str__(self):
        return self.title

class Genre(models.Model):
    name = models.CharField(max_length=200, unique=True)
    magazines = models.ManyToManyField(Magazine, related_name="magazine")

class Issue(models.Model):
    magazine = models.ForeignKey(Magazine, related_name="issues", on_delete=models.CASCADE)
    title = models.CharField(max_length=200, null=True)
    issue_number = models.SmallIntegerField(null=True)
    cover_image = models.URLField(null=True, blank=True)
    date_published = models.DateField()
    page_count = models.SmallIntegerField(null=True)
    description = models.CharField(max_length=500, null=True)

    def __str__(self):
        return f"{str(self.magazine)} issue: {str(self.issue_number)} {self.title}"

