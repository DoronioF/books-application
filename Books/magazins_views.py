from django.shortcuts import (get_object_or_404, render, redirect, HttpResponseRedirect)
from Books.models import (
    Magazine,
    Genre,
)
from Books.forms import MagazineForm



def show_magazines(request):
    magazines = Magazine.objects.all()
    context = {
        "magazines": magazines
    }
    return render(request, 'magazines/list.html', context)

def create_magazine(request):
    form = MagazineForm(request.POST or None)
    if form.is_valid():
        magazine = form.save()
        return redirect("show_magazines")
    context = {
        'form' : form
    }
    return render(request, "magazines/create.html", context)

def show_magazine(request, pk):
    magazine = Magazine.objects.get(pk=pk)
    context = {
        "magazine" : magazine,
    }

    return render(request, "magazines/detail.html", context)

def update_magazine(request, pk):
    context = {}
    magazine = get_object_or_404(Magazine, pk=pk)
    form = MagazineForm(request.POST or None, instance = magazine)
    if form.is_valid():
        form.save()
        return redirect("show_magazines")
    context["form"] = form

    return render(request, "magazines/update.html", context)

def delete_magazine(request, pk):
    context = {}
    magazine = get_object_or_404(Magazine, pk=pk)
    if request.method == "POST":
        magazine.delete()
        return redirect("show_magazines")

    return render(request, 'magazines/delete.html', context)

def view_genre(request, pk):
    genre = Genre.objects.filter(pk=pk)
    context = {
        "genre" : genre,
    }

    return render(request, "magazines/genre_details.html", context)